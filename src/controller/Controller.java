package controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import sample.Tile;
import javafx.scene.control.Button;

import java.awt.event.MouseEvent;
import java.util.*;


public class Controller {

    @FXML
    GridPane gPane;

    @FXML
    Button startButton;

    @FXML
    Label time;

    @FXML
    Label bombsLeft;

    Integer currentTime = 0;
    Integer bombsLeftInt = 100;
    int clearSpaces = 300;
    Timer timer = new Timer();

    Tile[][] grid = new Tile[20][20];
    boolean startGame = false;
    boolean gameOver = false;
    public javafx.scene.image.Image flagPNG = new javafx.scene.image.Image("https://openclipart.org/image/2400px/svg_to_png/213860/red_flag_13.png");
    public javafx.scene.image.Image bombPNG = new Image("http://pngimg.com/uploads/bomb/bomb_PNG26.png");
    public javafx.scene.image.Image questionPNG = new Image("http://www.emoji.co.uk/files/emoji-one/symbols-emoji-one/2099-black-question-mark-ornament.png");

    @FXML
    public void initialize()
    {
        ArrayList<Integer> cells = new ArrayList<Integer>();
        bombsLeft.setText(bombsLeftInt.toString());

        for (int i = 0; i < 400; i++)
        {
            cells.add(i);
        }
        Collections.shuffle(cells);
        int count = 0;

        for (int i = 0; i < 20; i++)
        {
            for (int j = 0; j < 20; j++) // creates locations for 20x20 buttons
            {
                Tile cell;
                if (cells.get(count) < 100)
                {
                    cell = new Tile(i,j,true);
                }
                else
                {
                    cell = new Tile(i,j,false);
                }
                count++;

                grid[i][j] = cell;

                // for testing
//                if (grid[i][j].bomb)
//                {
//                    ImageView temp = new ImageView(bombPNG);
//                    temp.setPreserveRatio(true);
//                    temp.setSmooth(true);
//                    temp.setFitHeight(15);
//                    temp.setFitWidth(15);
//                    cell.setGraphic(temp);
//                }

                gPane.add(cell,i,j);
                cell.setOnMousePressed(Event -> // event handler for each button
                {
                    if (!startGame) // the first button hasn't been pressed
                    {
                        startGame = true;
                        startButton.setDisable(true);
                        // start timer
                        if (!cell.bomb)
                        {
                            startTimer();
                        }
                    }
                    if (!cell.questioned && !cell.flagged)
                    {
                        if (Event.getButton() == MouseButton.PRIMARY) // left click
                        {
                            if (cell.bomb) {
                                // end game
                                // end timer
                                timer.cancel();

                                gameOver = true;
                                startGame = false;

                                startButton.setDisable(false);

                                for (int a = 0; a < 20; a++)
                                {
                                    for (int b = 0; b < 20; b++)
                                    {
                                        if (grid[a][b].bomb)
                                        {
                                            ImageView t = new ImageView(bombPNG);
                                            t.setPreserveRatio(true);
                                            t.setSmooth(true);
                                            t.setFitHeight(15);
                                            t.setFitWidth(15);
                                            grid[a][b].setGraphic(t);

                                            if (grid[a][b].flagged)
                                            {
                                                grid[a][b].setStyle("-fx-background-color: green");
                                            }
                                            else if (grid[a][b].neither)
                                            {
                                                grid[a][b].setStyle("-fx-background-color: red");
                                            }
                                        }
                                        else if (!grid[a][b].bomb)
                                        {
                                            if (grid[a][b].flagged)
                                            {
                                                grid[a][b].setStyle("-fx-background-color: yellow");
                                            }
                                        }
                                    }
                                }

                                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                alert.setTitle("GAME OVER");
                                alert.setHeaderText(null);
                                alert.setContentText("After " + currentTime + " seconds, you hit a bomb!");
                                alert.show();
                            }
                            else // not a bomb
                            {
                                if (!cell.clicked && countBombs(cell.x,cell.y) == 0)
                                {
                                    showNeighbors(cell.x, cell.y);
                                }
                                else{
                                    if (!cell.clicked)
                                    {
                                        clearSpaces--;
                                    }
                                    cell.setText(Integer.toString(countBombs(cell.x,cell.y)));
                                }
                            }
                            cell.clicked = true;

                            if (clearSpaces <= 0)
                            {
                                for (int a = 0; a < 20; a++) {
                                    for (int b = 0; b < 20; b++) {
                                        if (grid[a][b].bomb) {
                                            ImageView t = new ImageView(bombPNG);
                                            t.setPreserveRatio(true);
                                            t.setSmooth(true);
                                            t.setFitHeight(15);
                                            t.setFitWidth(15);
                                            grid[a][b].setGraphic(t);
                                            grid[a][b].setStyle("-fx-background-color: green");
                                        }
                                    }
                                }

                                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                alert.setTitle("GAME OVER");
                                alert.setHeaderText(null);
                                alert.setContentText("After " + currentTime + " seconds, you won!");
                                alert.show();

                                gameOver = true;
                                startGame = false;
                                timer.cancel();
                                startButton.setDisable(false);
                            }
                        }
                    }

                    if (Event.getButton() == MouseButton.SECONDARY) // right click
                    {
                       if (!cell.clicked)
                       {
                           if (cell.neither)
                           {
                               cell.neither = false;
                               cell.flagged = true;

                               bombsLeftInt --;
                               bombsLeft.setText(bombsLeftInt.toString());

                               ImageView temp = new ImageView(flagPNG);
                               temp.setPreserveRatio(true);
                               temp.setSmooth(true);
                               temp.setFitHeight(20);
                               temp.setFitWidth(20);
                               cell.setGraphic(temp);
                               //cell.setText("X");
                           }
                           else if (cell.flagged)
                           {
                               cell.questioned = true;
                               cell.flagged = false;

                               bombsLeftInt ++;
                               bombsLeft.setText(bombsLeftInt.toString());

                               ImageView temp = new ImageView(questionPNG);
                               temp.setPreserveRatio(true);
                               temp.setSmooth(true);
                               temp.setFitHeight(15);
                               temp.setFitWidth(15);
                               cell.setGraphic(temp);
                           }
                           else if (cell.questioned)
                           {
                               cell.neither = true;
                               cell.flagged = false;
                               cell.questioned = false;

                               cell.setGraphic(null);
                               cell.setText(" ");
                           }
                       }
                    }
                });
            }
        }
    }

    public void startTimer() {
        timer.scheduleAtFixedRate(new
          TimerTask() {
              @Override
              public void run ()
              {
                  Platform.runLater(() -> {
                      time.setText(null);
                      time.setText(currentTime.toString());
                      currentTime++;
                  });
              }
          },0,1000);
    }

    public void showNeighbors(int row, int col) {
        for (int x = row - 1; x <= row + 1; x++)
        {
            for (int y = col - 1; y <= col + 1; y++)
            {
                if (x >= 0 && y >= 0 && x <= 19 && y <= 19)
                {
                    if (countBombs(x,y) == 0 && !grid[x][y].clicked)
                    {
                        clearSpaces--;

                        grid[x][y].clicked = true;
                        grid[x][y].setStyle("-fx-background-color: darkgray");
                        showNeighbors(x,y);
                    }
                    else if (!grid[x][y].clicked) // if there is a bomb next to it
                    {
                        clearSpaces--;

                        grid[x][y].setText(Integer.toString(countBombs(x,y)));
                        grid[x][y].clicked = true;
                    }
                }

            }
        }
        return;
    }

    public int countBombs(int row, int col){
        int bombCount = 0;
        for (int x = row - 1; x <= row + 1; x++)
        {
            for (int y = col - 1; y <= col + 1; y++)
            {
                if (x >=0 && y >= 0 && x <=19 && y <= 19) {
                    if (grid[x][y].bomb) {
                        bombCount++;
                    }
                }
            }
        }
        return bombCount;
    }

    public void resetGame() {
        if (gameOver) {
            gameOver = false;
            currentTime = 0;
            bombsLeftInt = 100;
            clearSpaces = 300;
            startGame = false;
            bombsLeft.setText(bombsLeftInt.toString());
            timer = new Timer();
            time.setText("-");

            ArrayList<Integer> cells = new ArrayList<Integer>();

            for (int i = 0; i < 400; i++) {
                cells.add(i);
            }
            Collections.shuffle(cells);
            int count = 0;

            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 20; j++) // creates locations for 20x20 buttons
                {
                    if (cells.get(count) < 100) {
                        grid[i][j].bomb = true;
                    } else {
                        grid[i][j].bomb = false;
                    }
                    count++;

                    grid[i][j].setGraphic(null);
                    grid[i][j].setStyle(null);
                    grid[i][j].setText(null);
                    grid[i][j].clicked = false;
                    grid[i][j].flagged = false;
                    grid[i][j].questioned = false;
                    grid[i][j].neither = true;
                }
            }
        }
    }

}
