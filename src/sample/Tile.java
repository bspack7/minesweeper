package sample;

import javafx.scene.control.Button;
import javafx.scene.image.Image;

public class Tile extends Button {

    public int x;
    public int y;
    public boolean bomb;
    public boolean clicked = false;
    public boolean flagged = false;
    public boolean questioned = false;
    public boolean neither = true;

    public Tile(int x, int y, boolean bomb)
    {
        this.x = x;
        this.y = y;
        this.bomb = bomb;
        this.setPrefWidth(40);
    }
}
